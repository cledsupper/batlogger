#!/bin/sh
echo "[Desktop Entry]\
\nName=BatLogger\
\nName[pt_BR]=BatLogger\
\nGenericName=Battery Log Manager\
\nGenericName[pt_BR]=Gerenciador de Relatórios da Bateria\
\nComment=BatLogger creates battery logs and manages it\
\nComment[pt_BR]=O BatLogger cria relatórios da bateria e gerencia-os\
\nKeywords=battery;consumption;charge;bateria;carga;consumo;\
\nIcon=bateria-100%\
\nExec=$HOME/.batlogger/batloader\
\nPath=$HOME/.batlogger/\
\nTerminal=false\
\nTerminalOptions=\
\nType=Application\
\nCategories=System;Monitor;\
\nStartupNotify=true" > BatLogger.desktop

echo "[Desktop Entry]\
\nName=BatLogger Daemon\
\nGenericName=Battery Log Manager Daemon\
\nGenericName[pt_BR]=Serviço do Gerenciador de Relatórios da Bateria\
\nComment=Generates battery logs to BatLogger in the background\
\nComment[pt_BR]=Gera relatórios da bateria em segundo plano\
\nIcon=bateria-100%\
\nExec=$HOME/.batlogger/batautoloader.sh\
\nPath=$HOME/.batlogger/\
\nTerminal=false\
\nTerminalOptions=\
\nType=Application\
\nStartupNotify=false" > batloader.desktop
