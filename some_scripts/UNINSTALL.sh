#!/bin/sh
echo " ---- Desinstalação do BatLogger :( ----"
echo "Apagando arquivos de programa, configurações e etc...."
cd ~/.batlogger
./batloader stop

rm ~/.config/autostart/batloader.desktop
rm ~/.local/share/applications/BatLogger.desktop
rm ~/.local/share/icons/hicolor/*x*/apps/bateria-100%.png

rm ~/Desktop/BatLogger.desktop
rm ~/Área\ de\ Trabalho/BatLogger.desktop
cd $HOME
rm -r .batlogger

echo "\nPronto, tudo limpo!"
echo "by Ledso"
