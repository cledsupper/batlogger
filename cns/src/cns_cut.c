/* cns_cut.c - função para cortar C(strings)
 *
 * Copyright (C) 2018  Cledson Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __cplusplus
#include <stdio.h>
#else
#include <cstdio>
#endif

#define CNS_COMPILING 1
#include <cns/cns_cut.h>
#include <cns/cns_leftfel.h>

CNString* getCNSCut(const void* _string, long start, long end) {
    _CNS_AVOID_NULL_POINTER(_string, getCNSCut(), 1, NULL);
    bool doLeftfel = false;
    long i;
    CNString *cut;
    if (start > end) {
        cut = getCNString(_string, FinalArg);
        i = end;
        end = start+1;
        start = i+1;
        doLeftfel = true;
    } else if (start < end) cut = getCNString(_string, FinalArg);
    else return getCNString(FinalArg);

    if (!cut) {
        _CNS_WARNING(getCNSCut());
        return NULL;
    }
    if ((size_t)end >= cut->size || start < 0 || end < 0) {
        fprintf(stderr, "\n *** libCNString says: "
            "invalid limits to cut String!");
        fprintf(stderr, "\n ... a (%li)-(%li) range to a C(string) of size %lu\n",
                start, end, (unsigned long)cut->size);
        cns_delete(cut);
        return NULL;
    }

    for (i=0; i < start; i++)
        cns_remchar(cut, 0);
    cns_resize(cut, (size_t)(end-start)+1);

    CNString *tuc;
    if (doLeftfel) {
        tuc = getCNSLeftfel(cut);
        cns_delete(cut);
    } else tuc = cut;
    return tuc;
}
