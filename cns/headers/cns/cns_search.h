/* cns_search.h - pesquisa em C(strings)
 *
 * Copyright (C) 2018  Cledson Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _CNS_SEARCH_H
#define _CNS_SEARCH_H 1
#include <cnstring.h>

/** Pesquisa na String por 'str_seq' e retorna a quantidade de ocorrências, ou a
 * posição da <skip+1>ª ocorrência. "Ocorrência" é a quantidade de vezes que uma
 * C(string) foi encontrada dentro da String.
 * 
 * A variável <skip> significa quantas ocorrências devem ser ignoradas.
 * Exemplo:
 * Se <skip> == 1, _search() retorna a primeira posição da segunda ocorrência;
 * mas se <skip> é um número negativo, _search() retorna a quantidade de
 * ocorrências, que deve ser um número maior que 0.
 *
 * Retorna:
 * Para <skip> < 0, retorna o número de ocorrências:
 * ... inteiro: número de ocorrências de 'str_seq' em 'Str'. Se não existir, retorna
 * ... 0; se _seq é uma C(string) vazia, retorna o tamanho de Str.
 * Para <skip> >= 0:
 * ... inteiro >=  0: 'Str' contém 'str_seq' OU 'str_seq' é vazia;
 * ... inteiro == -1: caso 'Str' não contenha 'str_seq', ou ainda se 'Str' ou 'str_seq'
 * ... não existe(m).
 *
 * O valor retornado é long pois size_t > int.
 *
 * @param _string - C(string).
 * @param str_seq - uma C(string) para pesquisar em '_string'.
 * @return long - número de ocorrências ou a posição da <skip+1>ª ocorrência.
 */
long cns_search(const void *_string, const void *str_seq, int skip)
    __attribute__((nonnull));

#endif
