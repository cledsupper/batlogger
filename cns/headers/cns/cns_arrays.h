/* cns_arrays.h - funções para trabalho com arrays de cnstrings
 *
 * Copyright (C) 2018  Cledson Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _CNS_ARRAYS_H
#define _CNS_ARRAYS_H 1

/** Junta um array de cnstrings colocando 'str_seq' entre cada String.
 * O array deve ter um último elemento nulo.
 *
 * Obs.: libCNString ainda não suporta expressões regulares, str_seq então é uma
 * C(string) qualquer.
 *
 * @param list - um array de Strings terminado com NULL.
 * @param str_seq - uma C(string) ou NULL (apenas para performance).
 * @return String, NULL - erro.
 */
cnstring cns_join(cnstring list[], const void *str_seq) __attribute__((nonnull (1)));


/** Divide uma String em várias Strings a partir do separador 'str_seq'.
 * A última posição do array resultante é NULL, então o tamanho do array é obtido
 * a partir da posição de NULL + 1.
 *
 * Obs.: a menos que você crie uma String com malloc(), nenhuma String pode ser
 * excluída com free(). Use cns_delete() se quiser apagar as Strings do
 * vetor. O vetor de Strings pode e deve ser liberado com free().
 *
 * @param _string - C(string).
 * @param str_seq - C(string) com sequência de separação.
 * @return vetor de Strings que deve ser excluído com free(), NULL - erro.
 */
cnstring* cns_split(const void *_string, const void *str_seq) __attribute__((nonnull));


/** Remove todas as Strings contidas num vetor.
 *
 * @param list - vetor de Strings, com último elemento sendo um ponteiro NULL.
 * @return true, false - erro. */
bool cns_deleteall(cnstring list[]);

#endif