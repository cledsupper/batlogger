/* batloader.c - BatLogger Daemon e lançador de interface gráfica
 * version 0.6.0509 beta
 *
 * Copyright © 2018 Cledson Ferreira
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <cns_extras.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include "batloader.h"
#include "batlogger.h"

static const char BL_HELP[] = " ---- Ajuda do BatLoader - versão %s ---- \
\nSintaxe: ./batloader [opção] [stdout <arquivo>]\
\n\
\nTodos os parâmetros são opcionais. Se você executar:\
\n\t~/.batlogger$ ./batloader\
\nSerá executado tanto o Daemon (caso não esteja em execução), quanto o\
 BatLogger\nGUI (interface gráfica do BatLogger).\
\n\
\n\"stdout\": redireciona a saída para um arquivo, exemplo com \"X.txt\":\
\n\t~/.batlogger$ ./batloader help stdout \"X.txt\"\
\nEsta ajuda será salvada no arquivo \"X.txt\" do diretório corrente.\
\n\
\nOpções possíveis:\
\n\t silent, start:  executa apenas o daemon/serviço de relatórios (útil para\
\nquando o sistema operacional está iniciando)\
\n\t gui:            executa somente a interface gráfica do BatLogger (.jar)\
\n\t stop:           encerra o daemon do BatLogger\
\n\t help, <...>:    mostra esta ajuda\
\n\t remove-[cache|log]: remove o cache (veja batautoloader.sh) ou o relatório\
\n\t about, version: mostra a versão e outras informações do BatLogger\
\n\t site:           mostra o site de onde você poderá baixar atualizações (o\
Ledso Tips)\
\n\
\nComo encerrar o BatLogger Daemon:\
\n\t~/.batlogger$ ./batloader stop [stdout <arquivo>]\
\n\
\nObservação: a execução do BL Daemon é crucial para que as estatísticas da\
\nbateria sejam atualizadas, se você não se importa com isso, não se preocupe,\
\napenas executar \"%sbatloader\" no terminal já é o bastante para você.\
\n\
\nBUGS? SUGESTÕES? CRÍTICAS? --> %s <--\
\nApoie o projeto Upper, veja como em %s\n";

static const char BL_VERSION[] = "0.6.0509 beta";
static const char BL_ABOUT[] = "Lançador e daemon do BatLogger, versão %s.\
\nCopyright (C) 2018 Cledson Ferreira\n\
\nEste programa é software livre sob os termos da Licença MIT, acesse:\
\nhttps://github.com/cledsupper/batlogger/blob/master/COPYING para mais detalhes.\
\n\
\n\nO BatLoader utiliza a biblioteca de código aberto Glib, que é licenciada\
\npelos termos da GNU Lesser General Public License (LGPL) versão 2.1, acesse\
\nhttps://gitlab.gnome.org/GNOME/glib/blob/master/COPYING para mais detalhes.\n";

static const char DEVELOPERS_SITE[] = "https://ledsotips.blogspot.com/";
static const char DEVELOPERS_GITHUB[] = "https://github.com/cledsupper/";
static const char JAVA_NOT_INSTALLED_WARN[] = "\nA interface gráfica do BatLogger não iniciará \
    porque você não instalou o Java!\n";

const char *CACHE_FOLDER_PATH = "$HOME/.cache/BatLogger by Ledso/";
const char *CONFIG_FOLDER_PATH = "$HOME/.config/BatLogger by Ledso/";
const char *CONFIG_FILENAME = "batlogger.conf";
const char *DELETE_ME = "/tmp/PODE ME APAGAR SEU MERDA.nada";
const char *RUNNING_FILENAME = ".runningdaemon";
const char *LOG_FILENAME = "BLLog.log";

const char *SUPPLY_FOLDER_PATH = "/sys/class/power_supply/";
const char *CHARGE_NOW_FILENAME = "charge_now";
const char *CHARGE_FULL_FILENAME = "charge_full";
const char *VOLTAGE_NOW_FILENAME = "voltage_now";
const char *STATUS_FILENAME = "status";

const char *STATUS_FULL = "Full";
const char *STATUS_CHARGING = "Charging";
const char *STATUS_DISCHARGING = "Discharging";
const char *STATUS_NOT_CHARGING = "Not charging";
const char *STATUS_STOPPED = "Unknown";

cnstring lock_file_path = NULL; // este arquivo é exclusivo do daemon

cnstring cache_folder_path = NULL;
cnstring config_folder_path = NULL;
cnstring supply_folder_path = NULL;
cnstring supply_capacity_path = NULL; // pode ser o endereço de "charge_now" ou "capacity"
cnstring capacity_filename = NULL; // mesma coisa do seu endereço, mas apenas o nome do arquivo

int delay_time = DAEMON_SLEEP_TIME;

/* Todos os erros e mensagens comuns serão escritos neste arquivo */
FILE *output = NULL;

void bl_exit(int status) {
    if (output) {
        printf(" ------------- FINAL DO RELATÓRIO -----------------\n");
        fclose(output);
    }
    exit(status);
}

cnstring get_path(const char command_path[]) {
    cn_life(cnstring) command =
            cnstring("echo \"", command_path, "\" > \"", DELETE_ME, "\"");
    system(cns_get(command));
    FILE *fdelete_me = fopen(DELETE_ME, "r");
    if (!fdelete_me) {
        printf(ERROR_READ_TEMPORARY_FILE);
        cns_life_lose(command);
        bl_exit(FILE_NOT_FOUND_ERROR);
    }
    cns_freadline(command, fdelete_me);
    fclose(fdelete_me);
    return cns_life_get(command);
}

// retorna cnstring com o endereço do '.cache'
cnstring get_cachepath() {
    if (!cache_folder_path)
        cache_folder_path = get_path(CACHE_FOLDER_PATH);
    return cache_folder_path;
}

// retorna cnstring com o endereço do '.config'
cnstring get_configpath() {
    if (!config_folder_path)
        config_folder_path = get_path(CONFIG_FOLDER_PATH);
    return config_folder_path;
}

#ifdef TRIAL_PURPOSES
// para uso pela função posterior
int get_first_time(FILE *file) {
    int first_time;
    if (!file) return 0;
    cn_life(cnstring) time_str = cns_new();
    cns_freadline(time_str, file);
    first_time = cns_toint(time_str);
    return first_time;
}
#endif

// retorna a referência da string com o endereço dos arquivos de sistema
// ele declara a cnstring 'supply_capacity_path' também!
cnstring get_supplypath() {
    if (supply_folder_path)
        return supply_folder_path;

    cn_life(cnstring) config_path = cnstring(get_configpath());
    cns_addstr(config_path, CONFIG_FILENAME);
    FILE *config_file = fopen(cns_get(config_path), "r");
    if (!config_file) {
        printf("\nERRO: o arquivo de configuração não foi encontrado!\n");
        return NULL;
    }

    supply_folder_path = cns_new();
    cns_freadline(supply_folder_path, config_file);
    // Adicionar endereço/nome de arquivo que indica a capacidade também
    supply_capacity_path = cnstring(supply_folder_path);
    capacity_filename = cns_new();
    cns_freadline(capacity_filename, config_file);
    cns_append(supply_capacity_path, capacity_filename);
#ifdef TRIAL_PURPOSES
    // ler primeiro tempo de execução do programa
    time_t time_now = time(NULL)/60; // o tempo deve ser em minutos :?
    time_t first_time = get_first_time(config_file);
    if ((time_now - first_time) > TRIAL_DAYS_LIMIT) {
        printf("\nO prazo de teste foi expirado :(\n");
        printf("Considere apoiar o projeto Upper ou baixe e modifique o código fonte do");
        printf(" BatLogger para continuar usando a versão completa.\n");
        if (!lock_file_path) {
            lock_file_path = cnstring(supply_folder_path);
            cns_addstr(lock_file_path, RUNNING_FILENAME);
        }
        fclose(config_file);
        remove(lock_file_path->str);
        bl_exit(0);
    }
    else printf("\nVocê está usando uma versão de testes. %li minutos restantes\n",
        TRIAL_DAYS_LIMIT-(time_now-first_time));
#endif
    fclose(config_file);
    config_folder_path = cns_life_lose(config_folder_path);
    return supply_folder_path;
}

volatile bool do_exit = false;

/** Função chamada ao receber um sinal de interrupção, término ou desligamento */
void on_term(int signum) {
    if (do_exit) // isto não deve acontecer, mas se acontecer, encerra imediatamente
        exit(signum);
    do_exit = true; // apenas um sinal para encerrar corretamente
}

const char APP_PATH[] = "java -jar $HOME/.batlogger/BatLogger.jar &";
const char PROG_DEFAULT_PATH[] = "$HOME/.batlogger/";

/** Verifica se o programa está instalado corretamente e interrompe a execução se
 * houver um erro. */
void checkInstall() {
/* É possível testar o programa em um computador normal, para isto, você deve:
 * 
 * [1] CONFIGURAÇÃO DO BATLOGGER DAEMON
 * Escrever o arquivo "batlogger.conf" em "~/.config/BatLogger by Ledso/"
 * Nele, deve conter as duas linhas seguintes:
 * | [DIRETÓRIO DE SIMULAÇÃO DA BATERIA, POR EXEMPLO:]\
 * \ ~/Documentos/ROOT_EMULADO/sys/class/power_supply/battery/
 * | [ARQUIVO INDICADOR DE NÍVEL (%) OU DE CARGA (mAh) RESTANTE:] capacity
 * 
 * Três arquivos devem ser criados no diretório de simulação:
 * * capacity:    contendo apenas a porcentagem de carga, exemplo: 80;
 * * status:      pode ser qualquer coisa, inclusive "Charging" ou "Discharging".
 * * voltage_now: a tensão em µV (m*m Volt), exemplo: 16700000 (isto é, 16.7 V).
 * 
 * Após isso, basta executar o BL Daemon com:
 * $ mkdir -p build lib
 * $ make cnstring
 * $ make batloader
 * $ ./build/batloader start
 * [2] 
 */
#ifndef BL_ROOT_EMULATED
    cn_life(cnstring) comando = cnstring("test -e ", PROG_DEFAULT_PATH);
    int r = system(cns_get(comando));
    if (r) {
        printf("Huuumm, isso está me cheirando mal...\n");
        r = system("test -e BatLogger.jar");
        if (r) {
            cn_sleep(1);
            printf("\nEu sinto como se eu estivesse numa caçamba de lixo, onde "
                    "diabos tu me metestes?\n");
            cns_life_lose(comando);
            bl_exit(1);
        }
        cn_sleep(1);
    }
#endif
}

int main(int length, char* args[]) {
    char *fout_name;
    if (length>2) {
        if (!strcmp(args[1], "stdout")) {
            fout_name = args[2];
            output = fopen(fout_name, "a");
        } else if (!strcmp(args[2], "stdout") && length>3) {
            fout_name = args[3];
            output = fopen(fout_name, "a");
        }
    }
    if (output) {
        fclose(output);
        output = freopen(fout_name, "a", stdout);
        time_t milis_now = time(NULL);
        struct tm t_now = *localtime(&milis_now);
        printf("\n --- INÍCIO DO RELATÓRIO DE %02d/%02d/%d ÀS %02dh%02d ---\n",
               t_now.tm_mday, t_now.tm_mon+1, t_now.tm_year+1900,
               t_now.tm_hour, t_now.tm_min);
    }
    if (length>1) {
        if (!strcmp(args[1],"silent") || !strcmp(args[1], "start")) {
            checkInstall();
            printf("Iniciando BatLogger Daemon...\n");
            daemon_init();
        }
        else if (!strcmp(args[1], "gui")) {
            checkInstall();
            printf("Verificando a instalação do Java...\n");
            if (system("java -version")) {
                printf(JAVA_NOT_INSTALLED_WARN);
                bl_exit(FILE_NOT_FOUND_ERROR);
            }
            else {
                printf("Ok\n");
                system(APP_PATH);
            }
        }
        else if (!strcmp(args[1], "stop")) {
            cn_life(cnstring) status = cns_new();
            if (!getBatteryStatus(status))
                bl_exit(1);
            lock_file_path = cnstring(get_cachepath(), RUNNING_FILENAME);
            FILE *lock_file = fopen(cns_get(lock_file_path), "r");
            if (lock_file) {
                fclose(lock_file);
                remove(cns_get(lock_file_path));
                printf("Pronto, aguarde algum momento...\n");
                if (!strcmp(cns_get(status), STATUS_FULL)
                    || !strcmp(cns_get(status), STATUS_STOPPED))
                    cn_sleep(DAEMON_DEEPSLEEP_TIME);
                else cn_sleep(DAEMON_SLEEP_TIME);
            } else printf("O BatLogger não está em execução\n");
            supply_capacity_path = cns_life_lose(supply_capacity_path);
            supply_folder_path = cns_life_lose(supply_folder_path);
            cache_folder_path = cns_life_lose(cache_folder_path);
            capacity_filename = cns_life_lose(capacity_filename);
            lock_file_path = cns_life_lose(lock_file_path);
        }
        else if (!strncmp(args[1], "remove-", 7)) {
            int r=0;
            cn_life(cnstring) cache_file_path = cnstring(get_cachepath());
            if (!strcmp(args[1]+7, "cache")) {
                cns_addstr(cache_file_path, "batloader.log");
                r = remove(cns_get(cache_file_path));
            }
            else if (!strcmp(args[1]+7, "log")) {
                cns_addstr(cache_file_path, LOG_FILENAME);
                r = remove(cns_get(cache_file_path));
            }
            else {
                printf("ERRO: opção \"%s\" não reconhecida!\n"
                        "Use \"cache\" ou \"log\" em vez disso.\n", args[1]+7);
                bl_exit(1);
            }
            if (!r) printf("Removido %s\n", (args[1]+7));
            else printf("Falha ao remover %s: %s\n",
                        args[1]+7, strerror(errno));
            cache_folder_path = cns_life_lose(cache_folder_path);
        }
        else if (!strcmp(args[1], "version") || !strcmp(args[1], "about"))
            printf(BL_ABOUT, BL_VERSION);
        else if (!strcmp(args[1], "site"))
            printf(DEVELOPERS_SITE);
        else
            printf(BL_HELP, BL_VERSION, PROG_DEFAULT_PATH, DEVELOPERS_GITHUB,
                   DEVELOPERS_SITE);
    } else {
        checkInstall();
        printf("Verificando a instalação do Java...\n");
        if (system("java -version"))
            printf(JAVA_NOT_INSTALLED_WARN);
        else {
            printf("Ok\n");
            system(APP_PATH);
        }
        printf("Iniciando BatLogger Daemon...\n");
        daemon_init();
    }
    if (output) {
        printf(" ------------- FINAL DO RELATÓRIO -----------------\n");
        fclose(output);
    }
    return 0;
}
