/* batloader.h - BatLogger Daemon e lançador de interface gráfica
 * version 0.5.0326 beta
 *
 * Copyright © 2018 Cledson Ferreira
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef __BATLOADER_H__
#define __BATLOADER_H__

/* Erros incorrigíveis mesmo com BatLogger GUI */
#define ERROR_READ_TEMPORARY_FILE\
 "ERRO: falha ao tentar ler o arquivo temporário!\n"
#define ERROR_FAILED_TO_READ_FILE\
 "ERRO: falha ao tentar ler o arquivo \"%s\"!\n"
#define ERROR_CANT_WRITE_LOG\
 "ERRO: não foi possível escrever no relatório!\n"
#define ERROR_CANT_DO_LOCK\
 "ERRO: não foi possível criar arquivo de bloqueio!\n"

/* Erro de execução do BL Daemon antes do BL GUI */
#define ERROR_WARN_USER_HOW_TO_END\
 "Se esta mensagem aparecer mais de 3 vezes, execute o BatLogger GUI"\
 " ou apague o arquivo \"%s\" do diretório \"%s\"\n"

#define DAEMON_SLEEP_TIME 15
#define DAEMON_DEEPSLEEP_TIME 45
#define DAEMON_MAX_ERRORS 30
#define TRIAL_DAYS_LIMIT 2880 /* dois dias */

#define FILE_NOT_FOUND_ERROR 404

const char *CACHE_FOLDER_PATH;
const char *CONFIG_FOLDER_PATH;
const char *CONFIG_FILENAME;
const char *DELETE_ME;
const char *RUNNING_FILENAME;
const char *LOG_FILENAME;

const char *SUPPLY_FOLDER_PATH;
const char *CHARGE_NOW_FILENAME;
const char *CHARGE_FULL_FILENAME;
const char *VOLTAGE_NOW_FILENAME;
const char *STATUS_FILENAME;

const char *STATUS_FULL;
const char *STATUS_CHARGING;
const char *STATUS_DISCHARGING;
const char *STATUS_NOT_CHARGING;
const char *STATUS_STOPPED;

// arquivo de bloqueio para impedir a execução de outras instâncias
cnstring lock_file_path;
cnstring cache_folder_path;
cnstring config_folder_path;
cnstring supply_folder_path;
cnstring supply_capacity_path; // pode ser o endereço de "charge_now" ou "capacity"
cnstring capacity_filename; // mesma coisa do seu endereço, mas apenas o nome do arquivo

int delay_time;

FILE *output;

volatile bool do_exit;

/** Procedimento de captura de sinal */
void on_term(int signum);

/** Encerra o Daemon fechando arquivo de relatório e limpando memória */
void bl_exit(int status) __attribute__((noreturn));

/** Recebe vomo parâmetro um endereço, ecoa via chamada de sistema num arquivo
 * temporário, lê e retorna */
cnstring get_path(const char command_path[]);

/** Retorna o diretório cache local */
cnstring get_cachepath();

/** Retorna o diretório de configuração local */
cnstring get_configpath();

/** Lê a configuração do BatLogger (GUI), o diretório de eventos do módulo
 * power_supply e o nome do arquivo de eventos da capacidade energética.
 * 
 * Retorna o endereço do diretório de eventos da bateria. */
cnstring get_supplypath();

#endif
