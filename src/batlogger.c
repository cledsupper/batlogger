/* batlogger.c - funções diversas do BatLogger Daemon
 * version 0.4.0318
 *
 * Copyright © 2018 Cledson Ferreira
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <signal.h>
#include <cns_extras.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include "batloader.h"
#include "batlogger.h"

int getBatteryChargeFull() {
    static cnstring charge_full_path = NULL;
    if (!charge_full_path) 
        charge_full_path = cnstring(get_supplypath(), CHARGE_FULL_FILENAME);

    FILE *charge_full_file = fopen(cns_get(charge_full_path), "r");
    if (charge_full_file==NULL) {
        printf(ERROR_FAILED_TO_READ_FILE,
               cns_get(charge_full_path));
        cns_delete(charge_full_path);
        charge_full_path = NULL;
        return -1;
    }
    int charge_full;
    fscanf(charge_full_file, "%d", &charge_full);
    fclose(charge_full_file);
    return charge_full;
}

double getBatteryPercent() {
    FILE *capacity_file = fopen(cns_get(supply_capacity_path), "r");
    if (!capacity_file) {
        printf(ERROR_FAILED_TO_READ_FILE,
               cns_get(supply_capacity_path));
        return -1.0;
    }
    double level = 0.0;
    int charge_now = 0;
    if (!strcmp(cns_get(capacity_filename), CHARGE_NOW_FILENAME)) {
        fscanf(capacity_file, "%d", &charge_now);
        fclose(capacity_file);
        charge_now/=1000;
        int charge_full = getBatteryChargeFull()/1000;
        level = charge_now*100.0/charge_full;
    } else {
        fscanf(capacity_file, "%d", &charge_now);
        fclose(capacity_file);
        level = charge_now;
    }
    return level;
}

double getBatteryVoltage() {
    static cnstring supply_voltage_path = NULL;
    double voltage_now=0;
    FILE *voltage_now_file;
    if (!supply_voltage_path) 
        supply_voltage_path = cnstring(get_supplypath(), VOLTAGE_NOW_FILENAME);

    voltage_now_file = fopen(cns_get(supply_voltage_path), "r");
    if (!voltage_now_file) {
        printf(ERROR_FAILED_TO_READ_FILE,
            cns_get(supply_voltage_path));
        cns_delete(supply_voltage_path);
        supply_voltage_path = NULL;
        return -1;
    }
    fscanf(voltage_now_file, "%lf", &voltage_now);
    fclose(voltage_now_file);
    return voltage_now/1000000;
}

/* Retorna o status da bateria
 * A referência de string passada como argumento será alterada e retornada caso já exista
 * Caso contrário, o cns_freadline() cria uma cnstring e a função retorna esta
 */
cnstring getBatteryStatus(cnstring status) {
    cn_life(cnstring) supply_status_path = cnstring(get_supplypath(),
                                                    STATUS_FILENAME);

    FILE *status_file = fopen(cns_get(supply_status_path), "r");
    if (!status_file) {
        printf(ERROR_FAILED_TO_READ_FILE,
               cns_get(supply_status_path));
        return NULL;
    }

    cns_freadline(status, status_file);
    fclose(status_file);
    return status;
}

/** Pega os sinais para encerramento de processo mais comuns */
void configure_signals() {
    signal(SIGINT, on_term);
    signal(SIGTERM, on_term);
    signal(SIGHUP, on_term);
}

void daemon_init() {
    /** Loop do relatório */
    void do_logging();
    cnstring path = get_cachepath();
    cn_life(cnstring) mkdir_command = cnstring("mkdir -p \"", path, "\"");

    /* Este é o arquivo que indicará se este programa já está em execução
     * E eu sei que já são muitos arquivos... */
    lock_file_path = cnstring(path, RUNNING_FILENAME);

    system(cns_get(mkdir_command));
    cns_println("Eu uso este diretório: ", path);

    FILE *runningdaemon = fopen(cns_get(lock_file_path), "r");
    if (!runningdaemon) {
        runningdaemon = fopen(cns_get(lock_file_path), "w");
        if (!runningdaemon) {
            printf(ERROR_CANT_DO_LOCK);
            bl_exit(FILE_NOT_FOUND_ERROR);
        }
    } else {
        cns_println("Eu não posso ser executado duas vezes!\n"
            "... mas se você tem certeza de que eu não estou executando,"
            " apague este arquivo: ", lock_file_path);
        cns_println("Você pode fazer isso pelo BatLogger GUI, na aba \"Ajuda\"");
        fclose(runningdaemon);
        return;
    }
    fclose(runningdaemon);

    // Criar diretório de configuração caso não tenha sido criado:
    path = get_configpath();
    cns_update(mkdir_command, "mkdir -p \"", path, "\"");
    system(cns_get(mkdir_command));
    printf("...e este também: %s\n", cns_get(path));

    printf("\nO BatLogger Daemon foi iniciado\n");
    fflush(stdout);
    do_logging();
    remove(cns_get(lock_file_path));
}

void do_logging() {
    int errors=0;
    time_t last_time;
    double last_percent, percent, voltage;
    FILE *runningdaemon = fopen(cns_get(lock_file_path), "r");
    FILE *log_file = NULL;
    cn_life(cnstring) new_status = cns_new();
    cn_life(cnstring) last_status = cnstring("nulo");
    cn_life(cnstring) log_path = cnstring(get_cachepath(), LOG_FILENAME);

    configure_signals();
    while(runningdaemon) {
        fclose(runningdaemon);
        fflush(stdout);
        cn_sleep(delay_time);
        /* Caso receba um SIG[TERM|INT|HUP], encerra o loop */
        if (do_exit) break;
        if(!get_supplypath()) {
            printf(ERROR_WARN_USER_HOW_TO_END, RUNNING_FILENAME,
                get_cachepath()->string_c);
            runningdaemon = fopen(lock_file_path->string_c, "r");

            if (++errors >= DAEMON_MAX_ERRORS)
                break;
            continue;
        } else {
            if (!getBatteryStatus(new_status))
                if (++errors >= DAEMON_MAX_ERRORS) break;
            percent = getBatteryPercent();
            voltage = getBatteryVoltage();
            if ((int)percent <= ((int)last_percent-1)
                    || (int)percent >= ((int)last_percent+1)
                    || !cns_equals(last_status, new_status)) {
                last_time = time(NULL);
                struct tm t_last = *localtime(&last_time);
                log_file = fopen(cns_get(log_path), "a");
                if (log_file == NULL) {
                    cns_print(ERROR_CANT_WRITE_LOG);
                    if(++errors >= DAEMON_MAX_ERRORS)
                        break;
                    continue;
                }
                fprintf(log_file, "%.1lf|%.3lf|%02d/%02d/%04d, %02dh%02dmin|%s\n", percent, voltage,
                    t_last.tm_mday, t_last.tm_mon+1, t_last.tm_year+1900, t_last.tm_hour,
                    t_last.tm_min, cns_get(new_status));
                fclose(log_file);
                last_percent=percent;
            }
            cns_copy(last_status, new_status);
            if (!strcmp(cns_get(new_status), STATUS_STOPPED)
                || !strcmp(cns_get(new_status), STATUS_FULL)) {
                    if (delay_time == DAEMON_SLEEP_TIME) {
                        delay_time = DAEMON_DEEPSLEEP_TIME;
                        cns_println("I'M IN A DEEP SLEEP...\n");
                    }
            } else if (delay_time == DAEMON_DEEPSLEEP_TIME) {
                cns_println("I'M UP :D\n");
                delay_time = DAEMON_SLEEP_TIME;
            }
        }
        runningdaemon = fopen(cns_get(lock_file_path), "r");
    }
    
    if (errors >= DAEMON_MAX_ERRORS)
        cns_println("Devido a 30 erros, o BatLogger foi encerrado!");
    else cns_println("O BatLogger Daemon foi encerrado");
}
