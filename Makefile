# Compilação e instalação do BatLogger em modo debug #
# Mude para gcc se não tens o Clang
CC=clang
AR_COMMAND=ar rcs
LIB_CONFIG=-Icns/headers -Wall -Werror
LIB_CONFIG_LINKER=-Icns/headers -Wall -Werror -pthread
# Relativo a libcnstring: #
# _VER: número de lançamento
# _VES: subversão
# _VEB: data de construção
LIBCNS_VER=0
LIBCNS_VES=11
LIBCNS_VEB=506
LIBCNSTRING_SO=libcnstring.so.$(LIBCNS_VER).$(LIBCNS_VES).$(LIBCNS_VEB)
LIBCNSTRING_V_SO=libcnstring-$(LIBCNS_VER).$(LIBCNS_VES).so
CNSTRING_V=cnstring-$(LIBCNS_VER).$(LIBCNS_VES)

all: cnstring batloader

install:
	./INSTALL.sh

uninstall:
	~/.batlogger/UNINSTALL.sh

batloader:
	$(CC) -o build/batloader src/*.c -Wl,-rpath=lib/ -Llib/ -l$(CNSTRING_V) $(LIB_CONFIG)

batloader-emulated:
	$(CC) -o build/batloader src/*.c -DBL_ROOT_EMULATED -Wl,-rpath=lib/ -Llib/ -l$(CNSTRING_V) $(LIB_CONFIG)

batloader-trial:
	$(CC) -o build/batloader src/*.c -DTRIAL_PURPOSES -Wl,-rpath=lib/ -Llib -l$(CNSTRING_V) $(LIB_CONFIG_LINKER)

cnstring:
	$(CC) -shared -fPIC cns/src/*.c -o lib/$(LIBCNSTRING_V_SO) $(LIB_CONFIG_LINKER)

cnstring-nowarnings:
	$(CC) -shared -fPIC cns/src/*.c -DCNS_SUPPRESS_WARNINGS -o lib/$(LIBCNSTRING_SO) $(LIB_CONFIG_LINKER)
	ln -s $(LIBCNSTRING_SO) $(LIBCNSTRING_V_SO)
	mv $(LIBCNSTRING_V_SO) lib/

batloader-static: cnstring-static
	$(CC) -static -o build/batloader batloader.c $(LIB_CONFIG_LINKER) -Llib/ -lcnstring

cnstring-static:
	$(CC) -c $(LIB_CONFIG) cns/src/cnstring.c    -o lib/cnstring.o
	$(CC) -c $(LIB_CONFIG) cns/src/cns_extras.c  -o lib/cns_extras.o
	$(CC) -c $(LIB_CONFIG) cns/src/cns_leftfel.c -o lib/cns_leftfel.o
	$(CC) -c $(LIB_CONFIG) cns/src/cns_cut.c     -o lib/cns_cut.o
	$(CC) -c $(LIB_CONFIG) cns/src/cns_search.c  -o lib/cns_search.o
	$(CC) -c $(LIB_CONFIG) cns/src/cns_arrays.c  -o lib/cns_arrays.o
	$(CC) -c $(LIB_CONFIG) cns/src/cns_string.c  -o lib/cns_string.o
	$(AR_COMMAND) lib/libcnstring.a lib/cns*.o

clean:
	rm -f build/* lib/*.so* lib/*.a lib/*.o

help:
	cat README
