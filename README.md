# Carregador e serviço do BatLogger
O carregador e daemon/serviço de gerenciamento automático de relatórios do BatLogger

É aqui que você encontra o código fonte do Batloader e o instalador do BatLogger (o conjunto BatLogger Loader and Daemon + GUI) para Linux.


### COMPILANDO

Para começar, baixe/clone esse repositório, você pode fazer isto em um diretório especial pelo terminal:
* git clone --recursive https://github.com/cledsupper/batlogger.git

É preciso que você tenha um compilador C instalado para compilar o código fonte do BatLoader. Eu gosto do Clang, mas se você não quiser instalá-lo, abra o arquivo Makefile em um editor de textos de sua preferência e troque a linha
* `CC=clang`  
para:  
* `CC=gcc`

Então, dentro do diretório do BatLogger, no terminal, construa tudo com o comando abaixo:  
* `make`


### DEPENDÊNCIAS

Na última atualização eu resolvi retirar do código fonte os bytecodes do BatLogger GUI e das suas bibliotecas, entretanto, você ainda pode baixar o código fonte do BatLogger GUI, abrir e construir o projeto no NetBeans IDE, só tendo que, antes de fazer isso, baixar manualmente a biblioteca JFreeChart e (provavelmente) substituir as jfreechart-*.jar e a jcommon-*.jar no NetBeans IDE pelas respectivas do arquivo compactado que você tenha extraído em algum lugar.

No meu caso, eu deixei a JFreeChart ao lado do fonte do BatLogger GUI, dentro do diretório NetbeansProjects. Além disso, os mesmos arquivos citados no parágrafo anterior devem estar dentro da pasta "lib", e o bytecode do BatLogger GUI deve estar dentro de "gui". Nesses diretórios eu deixei avisos em arquivos de texto, que podem ser úteis.


### INSTALANDO

A instalação é feita em modo de usuário, sem root.

Depois de compilar, você certamente vai querer testar o programa, então:  
* `make install`

### DESINSTALAÇÃO

Caso não queira mais usar o programa, entre no seu /home, abra a pasta do BatLogger e execute o script de desinstalação com:  
<pre><code>cd .batlogger
./UNINSTALL.sh</code></pre>

by cledsupper
