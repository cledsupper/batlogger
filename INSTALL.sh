#!/bin/sh
echo " ---- Instalador do BatLogger GUI + Daemon ----"
echo "Os arquivos estão sendo instalados no seguinte diretório:"
echo "$HOME/.batlogger"
mkdir -p ~/.batlogger/
echo "Instalando BatLoader... [executável]"
cp build/batloader ~/.batlogger/

echo "\nCopiando arquivos... [jar, .desktop, lib]"
mkdir -p ~/.batlogger/lib/
cp lib/jcommon-*.jar ~/.batlogger/lib/
cp lib/jfreechart-*.jar ~/.batlogger/lib/
mkdir -p ~/.local/share/icons/hicolor/16x16/apps
mkdir -p ~/.local/share/icons/hicolor/24x24/apps
mkdir -p ~/.local/share/icons/hicolor/32x32/apps
mkdir -p ~/.local/share/icons/hicolor/48x48/apps
mkdir -p ~/.local/share/icons/hicolor/256x256/apps
cp icon/16/bateria-100%.png ~/.local/share/icons/hicolor/16x16/apps/
cp icon/24/bateria-100%.png ~/.local/share/icons/hicolor/24x24/apps/
cp icon/32/bateria-100%.png ~/.local/share/icons/hicolor/32x32/apps/
cp icon/48/bateria-100%.png ~/.local/share/icons/hicolor/48x48/apps/
cp icon/64/bateria-100%.png ~/.local/share/icons/hicolor/64x64/apps/
cp icon/128/bateria-100%.png ~/.local/share/icons/hicolor/128x128/apps/
cp icon/256/bateria-100%.png ~/.local/share/icons/hicolor/256x256/apps/
cp icon/512/bateria-100%.png ~/.local/share/icons/hicolor/512x512/apps/
echo "Ignore os avisos acima de \"Arquivo ou diretório não encontrado\", se houverem"
cp gui/BatLogger.jar ~/.batlogger/
cp some_scripts/batautoloader.sh ~/.batlogger
cp some_scripts/UNINSTALL.sh ~/.batlogger/

echo "\nGerando lançadores..."
./some_scripts/generate-BatLogger.desktop.sh
chmod +x ./BatLogger.desktop
chmod +x ./batloader.desktop
cp BatLogger.desktop ~/.local/share/applications/
cp BatLogger.desktop ~/Área\ de\ Trabalho/
cp BatLogger.desktop ~/Desktop/
mkdir -p ~/.config/autostart/
cp batloader.desktop ~/.config/autostart/

echo "Em alguma versão do Linux, é necessário que o pacote Java seja executável..."
sleep 3
chmod +x ~/.batlogger/batloader
chmod +x ~/.batlogger/BatLogger.jar
chmod +x ~/.batlogger/batautoloader.sh
chmod +x ~/.batlogger/UNINSTALL.sh

echo "Limpando sujeira... ignore as mensagens de erro se aparecerem"
rm batloader.desktop BatLogger.desktop

echo "\nPronto, instalação terminada :D"
echo "O BatLogger GUI será executado automaticamente em 3 segs para fazer a configuração."
sleep 5
echo "\nExecutando o serviço do BatLogger..."
cd $HOME/.batlogger/
./batloader &
echo "by Ledso"
